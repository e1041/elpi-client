#!/bin/bash

set -e
set -x

WORKDIR=/home/elpi/opt

cd $WORKDIR/elpi-client
sudo bash install-package.sh

cd $WORKDIR
mkdir -p external
cd $WORKDIR/external
git clone https://github.com/orangepi-xunlong/wiringOP.git
cd $WORKDIR/external/wiringOP
./build clean
./build

cd $WORKDIR
wget "http://update.elpi-tech.ru/update/devcnf.tar.zst" -O devcnf.tar.zst
tar --zstd -xf $WORKDIR/devcnf.tar.zst -C $WORKDIR
cd $WORKDIR/devcnf/
bash build.sh -c

cd $WORKDIR/elpi-client
python3 elpi-client.py -install
