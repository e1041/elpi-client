"""
Данный модуль реализует проверку версии в json файлах программ.
"""

__author__ = 'Igor A. Shmakov'
__version__ = '0.3'
__license__ = "GPLv3"


import json
import elpiModule.connectSQL


def checkVersion(progPath="~/emonls", confName="info.json", verDB=1, fDEBUG=False):
    """
    Данный модуль получает номер версии из .json файла и сравнивает
    с версией в базе данных.

    args:
        progPath (str) - путь до программы;
        confName (str) - имя конфигурационного файла;
        verDB (str) - версия из базы;
        fDEBUG (bool) - вывод отладочной информации.
    return:
        retFlags:
            True - нужно произвести обновление;
            False - обновление не требуется.
    """
    eqVersion = False
    confPath = progPath + "/" + confName

    with open(confPath) as json_file:
        data = json.load(json_file)
        dbVers = verDB.split(", ")
        if (fDEBUG is True):
            print(verDB)
            print(dbVers)

        if (data["version"][0] != int(dbVers[0]) or data["version"][1] != int(dbVers[1]) or data["version"][2] != int(dbVers[2])):
            if (fDEBUG is True):
                print("Производим обновление файлов!")
            retFlags = True
        else:
            if (fDEBUG is True):
                print("Конфигурационные файлы не требуется обновлять!")
            retFlags = False

    return retFlags


def saveInfo(dictInfo, fDEBUG=False):
    """
    Модуль реализует сохранение информации, которая нужна для
    работы программы.

    args:
        dictInfo (dict) - словарь который нужно записать.
    """
    with open('elpi-client.json', 'w', encoding='utf-8') as f:
        json.dump(dictInfo, f, ensure_ascii=False, indent=4)


def readInfo(dictInfo, fDEBUG=False):
    """
    Модуль реализует чтение информации, которая нужна для
    работы программы.

    args:
        dictInfo (dict) - пустой словарь.

    return:
        dictInfo (dict) - возвращаемый словарь.
    """
    with open("elpi-client.json") as json_file:
        dictInfo = json.load(json_file)

    if (fDEBUG is True):
        print(dictInfo)

    return dictInfo


if __name__ == "__main__":
    """
    Модуль работы с json файлами.
    """
#    dbResult = connectSQL.directSQL(srvIP="192.168.0.206")
    dictInfo = dict()
#    for listLen in dbResult:
#        print(listLen["updateProgramm"])
#        print(listLen["updateConfig"])
#        print(listLen["versionProgram"])
#        print(listLen["versionConfig"])

    dictInfo["instalation"] = True
    saveInfo(dictInfo)
    dictInfo = readInfo(dictInfo)
    # print(checkVersion(progPath="example-json", confName="info.json"))
    # print(checkVersion(progPath="example-json", confName="config.json"))
