"""
Данный модуль реализует отправку файлов на почту.
"""

__author__ = 'Igor A. Shmakov'
__version__ = '0.1'
__license__ = "GPLv3"


import smtplib
from pathlib import Path
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formatdate
from email import encoders
from datetime import datetime, timedelta
import time
import os


def sendMail(send_from, send_to="logger@mail.elpi-tech.ru",
             subject="Send log file", message="Тестовое письмо.",
             files=[], server="mail.elpi-tech.ru", port=25,
             username='', password='', use_tls=True):
    """
    Модуль отправляет посьмо на сервер (mail.elpi-tech.ru),
    к данному письму можно приложить файлы.

    args:
        send_from (str): от кого отправляется письмо, hostname@elpi-tech.ru;
        send_to (str): для кого, по умолчанию logger@mail.elpi-tech.ru;
        subject (str): тема письма;
        message (str): сообщение;
        files (list[str]): список файлов;
        server (str): сервер по умолчанию mail.elpi-tech.ru;
        port (int): 25 порт;
        username (str): авторизация пока не реализована
        password (str): авторизация пока не реализована
        use_tls (bool): use TLS mode
    return:
        None.
    """
    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = send_to
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(message))

    for path in files:
        part = MIMEBase('application', "octet-stream")
        with open(path, 'rb') as file:
            part.set_payload(file.read())

            encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename={}'.format(Path(path).name))
        msg.attach(part)

    smtp = smtplib.SMTP(server, port)
    if use_tls:
        smtp.starttls()
        smtp.login(username, password)
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.quit()


def sendLog(hostname="example", logPath="/home/elpi/opt/example/log.txt",
            sendLogTime="23:30:00", name="example", dictInfo=dict(),
            fDEBUG=False, fForce=False):
    """
    Функция для отправки логов каждый день в определённое время.

    args:
        hostname (srt) - имя машины;
        logPath (str) - путь до логов;
        sendLogTime - время отправки логов;
        name (str) - имя программы;
        dictInfo (dict) - словарь;
        fDEBUG (bool) - выводить ли информацию на экран;
        fForce (bool) - отправлять ли принудительно логи/статистику.
    flags:
        sendFlags:
            True - письмо отправлено;
            False - письмо не требуется отправлять.
        changeFlags:
            True - сбросить информацию об отправлении файла;
            False - Не изменять информацию о флаге.
    return:
        changeFlags:
            True - сбросить информацию об отправлении файла;
            False - Не изменять информацию о флаге.
    """
    timeNOW = datetime.now().time()
    timeNOW = (timeNOW.hour * 60 + timeNOW.minute) * 60 + timeNOW.second
    dMinutes = 30
    timeWDP = sendLogTime + timedelta(minutes=dMinutes)
    timeWDM = sendLogTime - timedelta(minutes=dMinutes)
    # print(timeNOW, "!!!", dMinutes, "!!!", timeNOW.total_seconds())
    timeWDM = timeWDM.total_seconds()
    timeWDP = timeWDP.total_seconds()

    if (((timeNOW >= timeWDM) and (timeNOW <= timeWDP) and (dictInfo["sendLogFlags"] is False)) or (fForce is True)):
        if (fDEBUG is True):
            print("Отправка логов!")
            print(timeNOW, timeWDM, timeWDP)

        timeHMS = '{0:%Y-%m-%d-%H-%M-%S}'.format(datetime.now())
        os.system("tar --zstd -cf /tmp/%s-%s.tar.zst %s" % (name,
                                                            timeHMS,
                                                            logPath))
        # os.system("rm %s" % (logPath))
        # os.unlink(logPath)
        os.system("sudo rm %s -rf" %(logPath))
        os.system("mkdir %s" % (logPath))
        logFile = "/tmp/%s-%s.tar.zst" % (name, timeHMS)
        sendMail(send_from=hostname,
                 send_to="logger@mail.elpi-tech.ru",
                 subject="Send log file",
                 message="Отправка ежедневной статистики.",
                 files=[logFile],
                 server="mail.elpi-tech.ru",
                 port=25,
                 use_tls=False)

        sendFlags = True
        if (fDEBUG is True):
            print("Логи отправлены на почту!")

        dictInfo["sendLogFlags"] = True
    else:
        if (fDEBUG is True):
            print("Отправка сообщения не производилась!")
        sendFlags = False

    if (timeNOW > timeWDP):
        changeFlags = True
        if (fDEBUG is True):
            print("Требуется изменить состояние флага!")

        dictInfo["sendLogFlags"] = False
    else:
        changeFlags = False

    return changeFlags


if __name__ == "__main__":
    hostname = "example@elpi-tech.ru"
    sendMail(send_from=hostname, subject="Send log files",
             message="Тестовое письмо", files=["/tmp/test.txt"], use_tls=False)
    print("Отправка сообщения завершена")
