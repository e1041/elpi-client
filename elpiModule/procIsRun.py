"""
Данный модуль реализует проверку и работу с запуском программ.
"""

__author__ = 'Igor A. Shmakov'
__version__ = '0.4'
__license__ = "GPLv3"


import psutil
import subprocess
import time
import os
import signal


def isElpiClientRun(fDEBUG=False):
    """
    Данный модуль проверяет запущен ли elpi-client.

    args:
        fDEBUG (bool) - выводить ли сообщения.
    retrun:
        refFlag (bool) - запущен ли процесс.
    """
    retFlag = False
    count = 0
    for process in psutil.process_iter():
        # print(process.cmdline())
        if (("elpi-client.py" in process.cmdline()) or ("-update" in process.cmdline())):
            if (fDEBUG is True):
                print("Клиент был запущен!")
                print(process.name())
            count += 1

    if (count == 0 or count == 1):
        retFlag = False
    else:
        retFlag = True
    # print(count)
    return retFlag


def startProgram(progPath="~/emonls", progName="emonls", fDEBUG=False):
    """
    Данный модуль проверяет работает ли программа, если нет, то
    производит его запуск.

    args:
        progPath (str) - путь до исполняемого файла;
        progName (str) - имя программы, которое проверяется;
        fDEBUG (bool) - выводить ли сообщения.
    return:
        None - ничего не возвращает.
    """
    isRun = False
    for process in psutil.process_iter():
        if progName in process.name():
            if (fDEBUG is True):
                print("Программа была запущена!")
                print(process.name())
            isRun = True

    if (isRun is False):
        if (fDEBUG is True):
            print("Осуществляется запуск программы!")
        # subprocess.run(['cd', progPath], shell=True)
        runPath = progPath + "/" + progName
        # p = subprocess.Popen(runPath, cwd=progPath, close_fds=True)
        print(runPath)
        subprocess.run(['cd %s; sudo %s &' % (progPath, runPath)], shell=True)
        time.sleep(1)
        # os.system("%s &" % runPath)

    return None


def softStop(progName="emonls", fDEBUG=False):
    """
    Данный модуль отправляет signal.SIGINT программе.

    args:
        progName (str) - имя программы;
        fDEBUG (bool) - выводить ли сообщения.
    return:
        None - ничего не возвращает.
    """
    running = True
    for process in psutil.process_iter():
        if progName in process.name():
            progPid = process.pid
            if (fDEBUG is True):
                print("process.name", process.name())
                print("softStop", progName, "progPid", progPid)
            # os.kill(progPid, signal.SIGINT)
            os.system("sudo kill -2 %d" % (progPid))
            while running:
                if int(progPid) not in psutil.pids():
                    running = False
                    time.sleep(1)

    return None


if __name__ == "__main__":
    # startProgram()
    print("Проверка запущена программа или нет!")
    # print(softStop(progName="manager-testprog1"))
    print(isElpiClientRun(fDEBUG=True))
    
