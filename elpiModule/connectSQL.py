"""
Данный модуль реализует способы подключения к базе данных.
"""

__author__ = 'Igor A. Shmakov'
__version__ = '0.8'
__license__ = "GPLv3"

import pymysql
import os


def directSQL(srvIP="localhost", username="lrelpi",
              password="lrdevicePassword", db="elpi",
              hostname="example"):
    """
    Модуль реализует подключение (прамое к базе) и получает необходимые
    данные по имени машины (точки).
    args:
        srvIP (str) - ip адрес базы данных;
        username (str) - имя пользователя, у которого есть доступ на чтение;
        password (str) - пароль для досупа к базе;
        db (str) - имя базы данных из которой требуется получить данные;
        hostname (str) - имя машины (точки) для которой нужно получить данные.
    return:
        retString (list of dict) - возвращает словарь с данными.
    """
    conn = pymysql.connect(host=srvIP, user=username,
                           passwd=password, database=db,
                           cursorclass=pymysql.cursors.DictCursor)
    cur = conn.cursor()
    cur.execute("SELECT hostname, location.TZ, deviceCommand.*, \
    programm.name, programm.versionProgram, programm.versionConfig, \
    programm.urlProgram, programm.urlConfig, programm.localProgramPath, \
    programm.localConfigPath, programm.updateProgram, programm.updateConfig, \
    programm.sendLog, programm.restartTIME, programm.startProgramm \
    FROM device \
    JOIN location \
    ON device.location_locationID = location.locationID \
    JOIN programm \
    ON device.deviceID = programm.Device_deviceID \
    JOIN deviceCommand \
    ON programm.deviceCommandID = deviceCommand.deviceCommandID \
    WHERE device.hostname = %s", hostname)
    retString = list()
    for r in cur:
        retString.append(r)
#        print(r[2])
    cur.close()
    conn.close()

    return retString


def updateDirectSQL(srvIP="localhost", username="lrelpi",
                    password="lrdevicePassword", db="elpi",
                    hostname="example",
                    outmsg="example",
                    keys="updateDeviceGit",
                    commandDict=""):
    """
    """
    conn = pymysql.connect(host=srvIP, user=username,
                           passwd=password, database=db)
    cur = conn.cursor()
    # print("UPDATE `deviceCommand` SET `{0}` = '{1}', \
    # `commandOutput` = '{2}' WHERE `deviceCommand`.`deviceCommandID` = '{3}'".format(keys,
    #                                                                            commandDict[keys],
    #                                                                            outmsg,
    #                                                                            commandDict["deviceCommandID"]))
    cur.execute("UPDATE `deviceCommand` SET `{0}` = '{1}', \
    `commandOutput` = '{2}' WHERE `deviceCommand`.`deviceCommandID` = '{3}'".format(keys,
                                                                                    commandDict[keys],
                                                                                    outmsg,
                                                                                    commandDict["deviceCommandID"]))
    conn.commit()
    cur.close()
    conn.close()


def webSQL(srvIP="localhost", hostname="example"):
    pass


if __name__ == "__main__":
    # retString = directSQL(srvIP="192.168.0.206")
    hostname = os.uname()
    hostname = hostname[1]
    retString = directSQL(srvIP="update.elpi-tech.ru", hostname=hostname)
    print(retString)
    print("retString[0] ", len(retString))
    for listLen in range(0, len(retString)):
        for keys in retString[listLen]:
            print(listLen, keys, retString[listLen][keys])
#    print()
