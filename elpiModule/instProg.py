"""
Данный модуль реализует начальню установку программ.
"""

__author__ = 'Igor A. Shmakov'
__version__ = '0.5'
__license__ = "GPLv3"


import os
import errno
import elpiModule.connectSQL
import elpiModule.checkJSON as checkJSON
from tzlocal import get_localzone


def checkTZ(dbTZ, sPWD="example"):
    """
    args:
        dbTZ - временная зона полученная из БД.
    return:
        flag - значение флага:
            True - если временные зоны совпадают;
            False - если не совпадают.
    """
    tz = str(get_localzone())
    flag = False

    if (tz == dbTZ):
        flag = True
    else:
        flag = False
        command = ' timedatectl set-timezone' + dbTZ
        p = os.system('echo %s|sudo -S %s' % (sPWD, command))

    return flag


def checkDirExistOrCreate(dirPath="/home/elpi/opt/", fDEBUG=False):
    """
    Модуль проверяет существование директории,
    при отсутствии создаёт её.

    args:
        dirPath (str) - путь до директории.
    return:
        None
    """
    try:
        os.mkdir(dirPath)
        if (fDEBUG is True):
            print("Директория отсутствовала! ", dirPath)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise
        if (fDEBUG is True):
            print("Директория уже была!", dirPath)


def updateProgram(programmName, urlProgramm, listLen,
                  lPP="/tmp/elpi", dbResult=dict(),
                  fDEBUG=False, fForce=False):
    """
    Данный модуль реализует обновление программы.

    args:
        programmName (str) - имя программы для установки;
        urlProgramm (str) - путь до архива с программой;
        lPP (str) - установочный путь программы;
    return:
        retFlag (bool) - True / False.
    """
    pPath = dbResult[listLen]["localProgramPath"] + "/bin/" + dbResult[listLen]["nameProgramm"]
    retFlag = checkJSON.checkVersion(progPath=pPath,
                                     confName="info.json",
                                     verDB=dbResult[listLen]["versionProgram"],
                                     fDEBUG=False)
    return retFlag


def updateConfig(programmName, urlConfig, listLen,
                 lCP="/tmp/elpi", dbResult=dict(),
                 fDEBUG=False, fForce=False):
    """
    Данный модуль реализует обновление программы.
    args:
        programmName (str) - имя программы для установки;
        urlConfig (str) - путь до конфигурационного файла;
        lPP (str) - установочный путь программы;
    return:
        retFlag (bool) - True / False.
    """
    pPath = dbResult[listLen]["localProgramPath"] + "bin/" + dbResult[listLen]["nameProgramm"] + "/config"
    retFlag = checkJSON.checkVersion(progPath=pPath,
                                     confName="config.json",
                                     verDB=dbResult[listLen]["versionConfig"],
                                     fDEBUG=False)
    return retFlag


def installProgram(programmName, urlProgramm, urlConfig,
                   lPP="/tmp/elpi", fDEBUG=False):
    """
    Данный модуль устанавливает программы на устройство.

    args:
        programmName (str) - имя программы для установки;
        urlProgramm (str) - путь до архива с программой;
        urlConfig (str) - путь до конфигурационного файла;
        lPP (str) - установочный путь программы;
        lCP (str) - установочный путь конфигурационного файла;
    return:
        None - ничего не возвращает.
    """
    if (fDEBUG is True):
        print(programmName, urlProgramm, urlConfig, lPP)

    checkDirExistOrCreate(lPP, fDEBUG=True)
    checkDirExistOrCreate(str(lPP + programmName), fDEBUG=True)
    archivePath = lPP + "archive"
    checkDirExistOrCreate(archivePath, fDEBUG=True)
    configPath = lPP + "config"
    checkDirExistOrCreate(configPath, fDEBUG=True)
    configPathWName = configPath + "/" + programmName
    checkDirExistOrCreate(configPathWName, fDEBUG=True)

    # Производится загрузка архива с программой.
    os.system("wget -q -c -4 %s -O %s/%s.tar.zst" % (urlProgramm,
                                                     archivePath,
                                                     programmName))
    # Производится разархивация программы.
    # tar --zstd -cf emonls.zst vismon-gate/
    # tar --zstd -xf emonls.tar.zst -C emonls/
    # tar --zstd -xf test1.tar.zst -C /home/elpi/opt/
    os.system("tar --zstd -xf %s/%s.tar.zst -C %s" % (archivePath,
                                                      programmName,
                                                      lPP))
    # Скачивается конфигурационный файл для программы.
    os.system("wget -q -c -4 %s -O %s/config.tar.zst" % (urlConfig,
                                                         configPathWName))

    os.system("tar --zstd -xf %s/config.tar.zst -C %s" % (configPathWName,
                                                          configPathWName))

    os.system("cd %s%s/; bash build.sh -c" % (lPP, programmName))

    checkDirExistOrCreate(str(lPP + "bin"), fDEBUG=True)
    os.system("mv %s%s/mbuild %s/bin/%s" % (lPP,
                                            programmName,
                                            lPP,
                                            programmName))

    os.system("cp %s%s/info.json %s/bin/%s/" % (lPP,
                                                programmName,
                                                lPP,
                                                programmName))

    os.system("rm %s/bin/%s/config/ -rf" % (lPP, programmName))
    checkDirExistOrCreate(str(lPP + "bin/" + programmName + "/config"), fDEBUG=True)    
    os.system("mv %s/config/* %s/bin/%s/config/" % (configPathWName,
                                                    lPP,
                                                    programmName))

    if (fDEBUG is True):
        print(lPP, programmName, configPathWName)
    #    print("Запуск установки не производился!")


if __name__ == "__main__":
    dbResult = connectSQL.directSQL(srvIP="update.elpi-tech.ru")
    for listLen in range(0, len(dbResult)):
        if (dbResult[listLen]["nameProgramm"] == "example"):
            pass
        else:
            pN = dbResult[listLen]["nameProgramm"]
            uP = dbResult[listLen]["urlProgram"]
            uC = dbResult[listLen]["urlConfig"]
            iP = "/tmp/elpi"
            installProgram(pN, uP, uC, iP, fDEBUG=True)
