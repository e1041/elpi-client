"""
Данный модуль реализует интерфейс работы с менеджером elpi-py-client.
"""

__author__ = 'Igor A. Shmakov'
__version__ = '0.7'
__license__ = "GPLv3"

import sys
import os
import subprocess
import elpiModule.connectSQL as connectSQL
import elpiModule.sendMail as sendMail
import elpiModule.procIsRun as procIsRun
import elpiModule.instProg as instProg
import elpiModule.checkJSON as checkJSON
from tzlocal import get_localzone


if __name__ == "__main__":
    """
    args:
        1-й аргумент отвечает за ключ, например
        "-help" выдаст информационное сообщени, а "-test"
        произведёт отладочное подключение через directSQL.
    """
    # if (procIsRun.isElpiClientRun(fDEBUG=True) is True):
    #    exit()
    hostname = os.uname()
    hostname = hostname[1]
    sendName = str(hostname) + "@elpi-tech.ru"
    dbResult = connectSQL.directSQL(srvIP="update.elpi-tech.ru",
                                    hostname=hostname)
    dictInfo = dict()
    fDEBUG = False
    if (len(sys.argv) <= 1):
        print("Для вывода информационного сообщения \
        используйте python3 elpi-py-client.py -help")

    elif (sys.argv[1] == "-help"):
        print("Вывод этого информационного сообщения.")
        print("В программе реализованы следующие ключи:")
        print("-help - вывод этого информационного сообщения.")
        print("-test - отладочное подключение через directSQL.")
        print("-tz - проверяет временную зону системы и информацию в БД.")
        print("-version - выводи информацию о версиях модулей.")
        print("-git - обновление файлов локального менеджера.")
        print("-mail - отправка тестового сообщения.")
        print("-normal - запуск программы, основной режим работы.")
        print("-install - запуск программы, первичная установка программ.")
        print("-sendLog - отправка логов/статистики.")
        print("-update - проверка обновления файлов программ.")
        print("-run - запуск всех программ, которые разрешены на запуск.")

    elif (sys.argv[1] == "-test"):
        print(connectSQL.directSQL(srvIP="update.elpi-tech.ru",
                                   hostname=hostname))

    elif (sys.argv[1] == "-tz"):
        # dbResult = connectSQL.directSQL(srvIP="update.elpi-tech.ru", hostname=hostname)
        if (checkTZ((dbResult[0]["TZ"])) is True):
            print("TZ совподают!")
        else:
            print("Временная зона была обновлена!")

    elif (sys.argv[1] == "-version"):
        print("elpi-py-client.py -", __version__)
        print("connectSQL.py -", connectSQL.__version__)
        print("sendMail.py -", sendMail.__version__)
        print("procIsRun.py -", procIsRun.__version__)
        print("instProg.py -", instProg.__version__)
        print("checkJSON.py -", checkJSON.__version__)

    elif (sys.argv[1] == "-git"):
        print("Обновление файлов локального менеджера!")
        os.system('git pull')

    elif (sys.argv[1] == "-mail"):
        sendMail.sendMail(send_from=hostname, subject="Send testing mail",
                          message="Тестовое письмо", use_tls=False)

    elif (sys.argv[1] == "-sendLog"):
        dictInfo = checkJSON.readInfo(dictInfo, fDEBUG=fDEBUG)
        dictInfo["sendLog"] = str(dbResult[0]["sendLog"])
        try:
            temp = sys.argv[2]
            tempName = sys.argv[3]
        except IndexError:
            fForce = False

        if (sys.argv[2] == "-force"):
            fForce = True
        elif (sys.argv[2] == "-n"):
            fForce = False
        else:
            print("Неизвестный второй аргумент")

        for listLen in range(0, len(dbResult)):
            if (dbResult[listLen]["nameProgramm"] == tempName):
                logP = dbResult[listLen]["localProgramPath"] + "bin/" + dbResult[listLen]["name"] + "/statistics/"
                if (fDEBUG is True):
                    print(logP, dbResult[listLen]["sendLog"])

                    retFlags = sendMail.sendLog(hostname=sendName, logPath=logP,
                                                sendLogTime=dbResult[listLen]["sendLog"],
                                                name=dbResult[listLen]["name"],
                                                dictInfo=dictInfo,
                                                fDEBUG=fDEBUG,
                                                fForce=fForce)
        checkJSON.saveInfo(dictInfo)

    elif (sys.argv[1] == "-update"):
        dictInfo = checkJSON.readInfo(dictInfo, fDEBUG=fDEBUG)
        try:
            temp = sys.argv[2]
            tempName = sys.argv[3]
            fForce = True
        except IndexError:
            fForce = False

        if (sys.argv[2] == "-force"):
            fForce = True
        elif (sys.argv[2] == "-n"):
            fForce = False
        else:
            print("Неизвестный второй аргумент")

        for listLen in range(0, len(dbResult)):
            if ((dbResult[listLen]["updateProgram"] == 1) or
                (dbResult[listLen]["updateConfig"] == 1) or
                ((fForce is True) and
                 (dbResult[listLen]["nameProgramm"] == tempName))):
                retPFlag = instProg.updateProgram(programmName=dbResult[listLen]["nameProgramm"],
                                                  urlProgramm=dbResult[listLen]["urlProgram"],
                                                  listLen=listLen,
                                                  lPP=dbResult[listLen]["localProgramPath"],
                                                  dbResult=dbResult,
                                                  fDEBUG=True,
                                                  fForce=False)
                lCP = dbResult[listLen]["localProgramPath"] + "/bin/" + dbResult[listLen]["nameProgramm"] + "/config/"
                print("lCP =", lCP)
                retCFlag = instProg.updateConfig(programmName=dbResult[listLen]["nameProgramm"],
                                                 urlConfig=dbResult[listLen]["urlConfig"],
                                                 listLen=listLen,
                                                 lCP=lCP,
                                                 dbResult=dbResult,
                                                 fDEBUG=True,
                                                 fForce=False)

                print(retPFlag, retCFlag)
                urlProgramm = dbResult[listLen]["urlProgram"]
                urlConfig = dbResult[listLen]["urlConfig"]
                lPP = dbResult[listLen]["localProgramPath"]
                lCP = dbResult[listLen]["localConfigPath"] + dbResult[listLen]["nameProgramm"]

                if (retPFlag is True):
                    if (fDEBUG is True):
                        print("Производим отправку логов/статистики!")
                        print("Производим обновление программы.")

                    # Остановка программы.
                    procIsRun.softStop(dbResult[listLen]["name"])

                    # Отправка логов/статистики программы.
                    # subprocess.run(['python3 elpi-client.py -sendLog -force %s' % (dbResult[listLen]["nameProgramm"])], shell=True)
                    os.system("mkdir /tmp/tmpconfig/")

                    # Копирование старых конфигурационных файлов.
                    os.system("cp %s/bin/%s/config/* /tmp/tmpconfig/ -r" % (lPP, dbResult[listLen]["nameProgramm"]))

                    # Удаление старых данных связанных с обновляемой программой.
                    os.system("rm -rf %s%s" % (lPP, dbResult[listLen]["nameProgramm"]))
                    os.system("rm -rf %s/bin/%s" % (lPP, dbResult[listLen]["nameProgramm"]))
                    os.system("rm -rf %s/archive/%s.tar.zst" % (lPP, dbResult[listLen]["nameProgramm"]))

                    # Производится загрузка архива с программой.
                    os.system("wget -q -c -4 %s -O %s/archive/%s.tar.zst" % (urlProgramm,
                                                                             lPP,
                                                                             dbResult[listLen]["nameProgramm"]))

                    # Производится разархивация программы.
                    os.system("tar --zstd -xf %s/archive/%s.tar.zst -C %s" % (lPP,
                                                                              dbResult[listLen]["nameProgramm"],
                                                                              lPP))

                    # Удаление старых конфигурационных файлов, которые находятся в архиве.
                    os.system("rm -rf %s/%s/mbuild/config/" % (lPP,
                                                               dbResult[listLen]["nameProgramm"]))

                    # Проверяем наличие директории поддиректории..
                    tempPath = lPP + "/bin/" + dbResult[listLen]["nameProgramm"]
                    instProg.checkDirExistOrCreate(tempPath, fDEBUG=False)

                    tempPath = lPP + "/bin/" + dbResult[listLen]["nameProgramm"] + "/config/"
                    instProg.checkDirExistOrCreate(tempPath, fDEBUG=False)

                    # Восстановление стархы конфигурационных файлов.
                    os.system("cp /tmp/tmpconfig/* %s/bin/%s/config/ -r" % (lPP,
                                                                            dbResult[listLen]["nameProgramm"]))

                    # Сборка программы.
                    os.system("cd %s%s/; bash build.sh -c" % (lPP, dbResult[listLen]["nameProgramm"]))

                    # Перемещение собранной программы в рабочую директорию.
                    os.system("mv %s%s/mbuild/* %s/bin/%s/" % (lPP,
                                                               dbResult[listLen]["nameProgramm"],
                                                               lPP,
                                                               dbResult[listLen]["nameProgramm"]))

                    # Копирование файла info.json программы в рабочую директорию.
                    os.system("cp %s%s/info.json %s/bin/%s/" % (lPP,
                                                                dbResult[listLen]["nameProgramm"],
                                                                lPP,
                                                                dbResult[listLen]["nameProgramm"]))

                    # Удаление временной директории.
                    os.system("rm -rf /tmp/tmpconfig/")

                else:
                    if (fDEBUG is True):
                        print("Проверка работы модуля.")

                if (retCFlag is True):
                    if (fDEBUG is True):
                        print("Производим обновление конфигурационных файлов.")
                    # Остановка программы.
                    procIsRun.softStop(dbResult[listLen]["name"])

                    # Отправка логов/статистики программы.
                    # subprocess.run(['python3 elpi-client.py -sendLog -force %s' % (dbResult[listLen]["nameProgramm"])], shell=True)

                    # Удаление старых конфигурационных файлов.
                    os.system("rm -rf %s/bin/%s/config/*" % (lPP,
                                                             dbResult[listLen]["nameProgramm"]))

                    # Удаление старого архива с конфигурационными файлами.
                    os.system("rm -rf %s" % (lCP))

                    # Проверка и если требуется восстановление директории.
                    instProg.checkDirExistOrCreate(lCP, fDEBUG=False)

                    # Загрузка архива с конфигурационными файла для программы.
                    os.system("wget -q -c -4 %s -O %s/config.tar.zst" % (urlConfig,
                                                                         lCP))

                    # Распаковка архива.
                    os.system("tar --zstd -xf %s/config.tar.zst -C %s" % (lCP,
                                                                          lCP))

                    # Проверка на наличие директории config рядом с программой.
                    tempPath = lPP + "/bin/" + dbResult[listLen]["nameProgramm"] + "/config/"
                    instProg.checkDirExistOrCreate(tempPath, fDEBUG=False)

                    # Перенос конфигурационных файлов в рабочую директорию.
                    os.system("mv %s/config/* %s/bin/%s/config/" % (lCP,
                                                                    lPP,
                                                                    dbResult[listLen]["nameProgramm"]))
                else:
                    if (fDEBUG is True):
                        print("Проверка работы модуля.")

                fullPath = dbResult[listLen]["localProgramPath"] + "bin/" + dbResult[listLen]["nameProgramm"]
                procIsRun.startProgram(progPath=fullPath,
                                       progName=dbResult[listLen]["nameProgramm"],
                                       fDEBUG=True)

    elif (sys.argv[1] == "-install"):
        lCF = "elpi-client.json"

        for listLen in range(0, len(dbResult)):
            for keys in dbResult[listLen]:
                # print(listLen, keys, dbResult[listLen][keys])
                if (keys == "TZ"):
                    # print(listLen, "TZ")
                    dictInfo["TZ"] = dbResult[listLen]["TZ"]
                    dictInfo["sendLog"] = str(dbResult[listLen]["sendLog"])
                    dictInfo["sendLogFlags"] = False
                    if (instProg.checkTZ((dbResult[listLen][keys]), sPWD="example") is True):
                        if (fDEBUG is True):
                            print("TZ совподают!")
                    else:
                        if (fDEBUG is True):
                            print("Временная зона была обновлена!")
        print("Осуществляется первичная установка программ.")
        print(dbResult)
        for listLen in range(0, len(dbResult)):
            print(listLen)
            print(dbResult[listLen]["nameProgramm"])
            pN = dbResult[listLen]["nameProgramm"]
            uP = dbResult[listLen]["urlProgram"]
            uC = dbResult[listLen]["urlConfig"]
            iPP = dbResult[listLen]["localProgramPath"]
            dictInfo["instalation"] = True

            checkJSON.saveInfo(dictInfo)
            print("dictInfo ", dictInfo)
            instProg.installProgram(pN, uP, uC, iPP, fDEBUG=True)

    elif (sys.argv[1] == "-run"):
        try:
            runName = sys.argv[2]
        except IndexError:
            runName = "Next"

        for listLen in range(0, len(dbResult)):
            if (dbResult[listLen]["nameProgramm"] == runName):
                fullPath = dbResult[listLen]["localProgramPath"] + "bin/" + dbResult[listLen]["nameProgramm"]
                print(listLen, fullPath)
                procIsRun.startProgram(progPath=fullPath,
                                       progName=dbResult[listLen]["nameProgramm"],
                                       fDEBUG=True)

    elif (sys.argv[1] == "-normal"):
        if (procIsRun.isElpiClientRun(fDEBUG=False) is True):
            print("Elpi-client уже запушен.")
            exit()
        dictInfo = checkJSON.readInfo(dictInfo, fDEBUG=False)
        for listLen in range(0, len(dbResult)):
            # os.system("python3 elpi-client.py -sendLog -n %s" % (dbResult[listLen]["name"]))
            # os.system("python3 elpi-client.py -update -n %s" % (dbResult[listLen]["name"]))
            # print(listLen, dbResult[listLen]["name"])
            # subprocess.run(['python3 elpi-client.py -sendLog -n %s' % (dbResult[listLen]["name"])], shell=True)
            subprocess.run(['python3 elpi-client.py -update -n %s' % (dbResult[listLen]["name"])], shell=True)
        for listLen in range(0, len(dbResult)):
            commandDict = dict()
            commandDict["nameProgramm"] = dbResult[listLen]["nameProgramm"]
            commandDict["reboot"] = dbResult[listLen]["reboot"]
            commandDict["run"] = dbResult[listLen]["run"]
            commandDict["restart"] = dbResult[listLen]["restart"]
            commandDict["kill"] = dbResult[listLen]["kill"]
            commandDict["updateDeviceProgram"] = dbResult[listLen]["updateDeviceProgram"]
            commandDict["updateDeviceConfig"] = dbResult[listLen]["updateDeviceConfig"]
            commandDict["updateDeviceGit"] = dbResult[listLen]["updateDeviceGit"]
            # commandDict["commandOutput"] = dbResult[listLen]["commandOutput"]
            commandDict["commandOutput"] = ""
            commandDict["deviceCommandID"] = dbResult[listLen]["deviceCommandID"]
            if (dbResult[listLen]["reboot"] == 1):
                print("reboot")
                procIsRun.softStop(dbResult[listLen]["name"])
                commandDict["reboot"] = 0
                commandDict["commandOutput"] += "reboot"
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="reboot",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)
                os.system("/sbin/reboot")

            elif (dbResult[listLen]["kill"] == 1):
                print("kill")
                procIsRun.softStop(dbResult[listLen]["name"])
                # procIsRun.startProg(dbResult[listLen]["name"])
                commandDict["kill"] = 0
                commandDict["commandOutput"] += "kill"
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="kill",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)

            elif (dbResult[listLen]["updateDeviceProgram"] == 1):
                print("updateDeviceProgram")
                procIsRun.softStop(dbResult[listLen]["name"])
                # os.system("python3 elpi-client.py -sendLog -force %s" % (dbResult[listLen]["name"]))
                # os.system("python3 elpi-client.py -update -force %s" % (dbResult[listLen]["name"]))
                # os.system("python3 elpi-client.py -run %s" % (dbResult[listLen]["name"]))
                subprocess.run(['python3 elpi-client.py -sendLog -force %s' % (dbResult[listLen]["name"])], shell=True)
                subprocess.run(['python3 elpi-client.py -update -force %s' % (dbResult[listLen]["name"])], shell=True)
                subprocess.run(['python3 elpi-client.py -run %s' % (dbResult[listLen]["name"])], shell=True)
                commandDict["updateDeviceProgram"] = 0
                commandDict["commandOutput"] += "updateDeviceProgram"
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="updateDeviceProgram",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)

            elif (dbResult[listLen]["updateDeviceConfig"] == 1):
                print("updateConfig")
                procIsRun.softStop(dbResult[listLen]["name"])
                # os.system("python3 elpi-client.py -sendLog -force %s" % (dbResult[listLen]["name"]))
                # os.system("python3 elpi-client.py -update -force %s" % (dbResult[listLen]["name"]))
                # os.system("python3 elpi-client.py -run %s" % (dbResult[listLen]["name"]))
                subprocess.run(['python3 elpi-client.py -sendLog -force %s' % (dbResult[listLen]["name"])], shell=True)
                subprocess.run(['python3 elpi-client.py -update -force %s' % (dbResult[listLen]["name"])], shell=True)
                subprocess.run(['python3 elpi-client.py -run %s' % (dbResult[listLen]["name"])], shell=True)
                commandDict["updateDeviceConfig"] = 0
                commandDict["commandOutput"] += "updateDeviceConfig"
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="updateDeviceConfig",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)

            elif (dbResult[listLen]["updateDeviceGit"] == 1):
                print(listLen, "updateDeviceGit")
                commandDict["updateDeviceGit"] = 0
                commandDict["commandOutput"] += "updateDeviceGit"
                os.system("python3 elpi-client.py -git")
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="updateDeviceGit",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)

            elif (dbResult[listLen]["restart"] == 1):
                print("restart")
                procIsRun.softStop(dbResult[listLen]["nameProgramm"])
                fullPath = dbResult[listLen]["localProgramPath"] + "bin/" + dbResult[listLen]["nameProgramm"]
                procIsRun.startProgram(progPath=fullPath,
                                       progName=dbResult[listLen]["nameProgramm"],
                                       fDEBUG=True)
                commandDict["restart"] = 0
                commandDict["commandOutput"] += "restart"
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="restart",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)

            elif ((dbResult[listLen]["run"] == 1) or (dbResult[listLen]["startProgramm"] == 1)):
                print("run")
                fullPath = dbResult[listLen]["localProgramPath"] + "bin/" + dbResult[listLen]["nameProgramm"]
                procIsRun.startProgram(progPath=fullPath,
                                       progName=dbResult[listLen]["nameProgramm"],
                                       fDEBUG=True)
                commandDict["run"] = 0
                commandDict["commandOutput"] += "run"
                connectSQL.updateDirectSQL(srvIP="update.elpi-tech.ru",
                                           hostname=hostname,
                                           keys="run",
                                           outmsg=commandDict["commandOutput"],
                                           commandDict=commandDict)

            else:
                continue

    else:
        print("Для вывода информационного сообщения \
        используйте python3 elpi-py-client.py -help")
