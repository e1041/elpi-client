#!/bin/bash

set -e
set -x

apt install python3-pymysql python3-psutil python3-tzlocal zstd git
apt install screen rsync wget
apt install cmake build-essential
