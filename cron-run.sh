#!/bin/bash

set -e
set -x

WORKDIR=/home/elpi/opt/elpi-client
# echo $WORKDIR
cd $WORKDIR
/usr/bin/python3 $WORKDIR/elpi-client.py -normal
